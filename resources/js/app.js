import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard);

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';



Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

import App from './view/App'
import Login from './pages/login'
import Home from './pages/home'
import Register from './pages/register'
import Rooms from './rooms/index'
import Addrooms from './rooms/add'
import EditRoom from './rooms/edit'
import Photos from './pages/photos'
import Users from './pages/users'
import Password from './pages/password'
import Bookings from './rooms/bookings'
import Forget from './pages/forget'
import Invoices from './rooms/invoices'




const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
        },
        {
            path: '/rooms',
            name: 'rooms',
            component: Rooms,
        },
        {
            path: '/rooms/add',
            name: 'addrooms',
            component: Addrooms,
        },
        {
            path: '/rooms/edit/:id',
            name: 'editroom',
            component: EditRoom,
        },
        {
            path: '/photos',
            name: 'photos',
            component: Photos,
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/password',
            name: 'password',
            component: Password,
        },
        {
            path: '/bookings',
            name: 'bookings',
            component: Bookings,
        },
        {
            path: '/forget',
            name: 'forget',
            component: Forget,
            meta: { hideNavigation: true }
        },
        {
            path: '/invoices',
            name: 'invoices',
            component: Invoices,
        },



    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

